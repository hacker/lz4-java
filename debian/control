Source: lz4-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Saif Abdul Cassim <saif.15@cse.mrt.ac.lk>,
 tony mancill <tmancill@debian.org>
Build-Depends:
 ant,
 ant-contrib-cpptasks (>= 1.0~b5-5~),
 ant-optional,
 bnd,
 debhelper-compat (= 13),
 default-jdk,
 ivy-debian-helper,
 junit4,
 libcarrotsearch-randomizedtesting-java (>= 2.1.17-4~),
 liblz4-dev,
 libmvel-java,
 librhino-java (>= 1.7.7.2-3~),
 libxxhash-dev,
 lz4,
 maven-repo-helper,
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/java-team/lz4-java
Vcs-Git: https://salsa.debian.org/java-team/lz4-java.git
Homepage: https://github.com/lz4/lz4-java
Rules-Requires-Root: no

Package: liblz4-java
Architecture: all
Depends: ${misc:Depends}, liblz4-jni
Description: LZ4 compression for Java
 LZ4 compression for Java, based on Yann Collet's work available at
 https://github.com/lz4/lz4. This library provides access to two compression
 methods that both generate a valid LZ4 stream: fast scan (LZ4) with a low
 memory footprint, very fast and reasonable compression ratio, and high
 compression (LZ4 HC) with medium memory footprint, rather slow and a good
 compression ratio
 .
 The streams produced by those two compression algorithms use the same
 compression format, are very fast to decompress and can be decompressed
 by the same decompressor instance.

Package: liblz4-jni
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: LZ4 compression for Java (JNI library)
 LZ4 compression for Java, based on Yann Collet's work available at
 https://github.com/lz4/lz4. This library provides access to two compression
 methods that both generate a valid LZ4 stream: fast scan (LZ4) with a low
 memory footprint, very fast and reasonable compression ratio, and high
 compression (LZ4 HC) with medium memory footprint, rather slow and a good
 compression ratio
 .
 The streams produced by those two compression algorithms use the same
 compression format, are very fast to decompress and can be decompressed
 by the same decompressor instance.
 .
 This package contains the architecture specific Java native interface part.
